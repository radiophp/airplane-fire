﻿using System;
using System.Text;
using System.Threading;
using AirPlanFire;

namespace airplaneFire.GameEngine
{
    internal static class ConsoleHelper
    {
        internal static readonly string[] AsciiArt = new string[]
        {
            @"                               ._                             ",
            @"                              |* ;                            ",
            @"            `*-.              |"":                            ",
            @"             \  \             |""                              ",
            @"              .  \            |   :                           ",
            @"              `   \           |                                ",
            @"               \   \          |    ;               +.         ",
            @"                .   \         |                   *._`-.      ",
            @"                `    \        |     :          .-*'  `. `.    ",
            @"                 _\    \.__..--**--...L_   _.-*'      .'`*'    ",
            @"                /  `*-._\   -.       .-**+._       .'         ",
            @"               :        ``*-._*.     \      _J.   .'           ",
            @"           .-*'`*-.       ;     `.    \    /   `.'             ",
            @"       .-*'  _.-*'.     .-'       `-.  `-.:   _.'`-.           ",
            @"    +*' _.-*'      `..-'             `*-. `**'      `-.        ",
            @"     `*'          .-'      ._            `*-._         `.      ",
            @"              .-'         `.`-.____..+-**""'         .*""`.    ",
            @"          ._.-'          _.-*'':$$$;._$              /     `.  ",
            @"       .-'  `.      _.-*' `*-.__T$P   `""**--..__    :        `. ",
            @" .'..-'       \_.-*'                            `""**--..___.-*'",
            @" `. `.    _.-*'                                                 ",
            @"   `. `:*'         Mohammad Inanloo Tayefe Yaghmorloo           ",
            @"     `. `.         Student No: B2210.033130                     ",
            @"       `*                                                      "
        };

        internal static void InitializeConsole()
        {
            if (OperatingSystem.IsWindows())
            {
                Console.OutputEncoding = Encoding.UTF8;
                Program.consoleWidth = Program.intendedMinConsoleWidth + 10;  // Add some margin to the width
                Program.consoleHeight = Program.intendedMinConsoleHeight + 5; // Add some margin to the height

                if (Program.consoleWidth < Console.WindowWidth)
                {
                    Program.consoleWidth = Console.WindowWidth;
                }

                if (Program.consoleHeight < Console.WindowHeight)
                {
                    Program.consoleHeight = Console.WindowHeight;
                }

                try
                {
                    Console.SetWindowSize(Program.consoleWidth, Program.consoleHeight);
                    Console.SetBufferSize(Program.consoleWidth, Program.consoleHeight);
                }
                catch
                {
                    // Ignore exceptions related to setting the window size
                }

                Console.Clear();
            }

            if (Console.OutputEncoding != Encoding.UTF8)
            {
                Console.OutputEncoding = Encoding.UTF8;
            }
        }

        internal static void AdjustWindowSize()
        {
            if (OperatingSystem.IsWindows())
            {
                Program.consoleWidth = Program.intendedMinConsoleWidth + 10;
                Program.consoleHeight = Program.intendedMinConsoleHeight + 5;

                try
                {
                    Console.SetWindowSize(Program.consoleWidth, Program.consoleHeight);
                    Console.SetBufferSize(Program.consoleWidth, Program.consoleHeight);
                }
                catch
                {
                    // Ignore exceptions related to setting the window size
                }
            }
        }

        internal static void ShowStartScreen()
        {
            Console.Clear();
            // Console.WriteLine("\x1b[33m"); // Set color to yellow
            foreach (var line in AsciiArt)
            {
                Console.WriteLine(line);
            }
            // Console.WriteLine("\x1b[0m"); // Reset color

            Console.WriteLine("Welcome to the AirPlanFire Game!");
            Console.WriteLine("Press [N] to start a new game");
            Console.WriteLine("Press [L] to load the last saved game");

            while (true)
            {
                var key = Console.ReadKey(true).Key;
                if (key == ConsoleKey.N)
                {
                    return;
                }
                else if (key == ConsoleKey.L)
                {
                    if (GameSaveManager.LoadGame(out long score, out float playerX, out float playerY))
                    {
                        Program.player.X = playerX;
                        Program.player.Y = playerY;
                        Program.score = score;

                        return;
                    }
                    else
                    {
                        Console.WriteLine("No saved game found. Starting a new game...");
                        return;
                    }
                }
            }
        }


        internal static void PromptUserToSave()
        {
            Console.Clear();
            Console.WriteLine("GAME PAUSED! Press [P] to resume...");
            Console.WriteLine("Press [S] to save the game now");

            while (Program.isPaused)
            {
                var key = Console.ReadKey(true).Key;
                if (key == ConsoleKey.P)
                {
                    Program.isPaused = false;
                    break;
                }
                else if (key == ConsoleKey.S)
                {
                    GameSaveManager.SaveGame(Program.score, Program.player.X, Program.player.Y);
                    DisplayMessage("Game saved successfully. Press [P] to resume...");

                }
            }

            Console.Clear(); // Clear the console after saving or resuming
        }

        internal static void DisplayMessage(string message)
        {
            //Console.Clear();
            Console.WriteLine(message);
            Thread.Sleep(2000);
            //Console.Clear();
        }

        internal static void SleepAfterRender()
        {
            TimeSpan sleep = TimeSpan.FromSeconds(1d / 120d) - Program.stopwatch.Elapsed;
            if (sleep > TimeSpan.Zero)
            {
                Thread.Sleep(sleep);
            }
            Program.stopwatch.Restart();
        }
    }
}
