﻿using airplaneFire;
using airplaneFire.GameEngine;
using airplaneFire.Utility;
using AirPlanFire.Enemies;
using System;
using System.Runtime.InteropServices;

namespace AirPlanFire
{
    internal static partial class GameUpdate
    {
        internal static void ShowGameOverArt()
        {
            Console.Clear();
            Console.WriteLine("\x1b[31m"); // Set color to red
            Console.WriteLine("   ____                         ___                 ");
            Console.WriteLine("  / ___| __ _ _ __ ___   ___   / _ \\__   _____ _ __ ");
            Console.WriteLine(" | |  _ / _` | '_ ` _ \\ / _ \\ | | | \\ \\ / / _ \\ '__|");
            Console.WriteLine(" | |_| | (_| | | | | | |  __/ | |_| |\\ V /  __/ |   ");
            Console.WriteLine("  \\____|\\__,_|_| |_| |_|\\___|  \\___/  \\_/ \\___|_|   ");
            Console.WriteLine("\x1b[0m"); // Reset color

            Console.WriteLine("Press [N] to start a new game");
            Console.WriteLine("Press [L] to load the last saved game");

            while (true)
            {
                var key = Console.ReadKey(true).Key;
                if (key == ConsoleKey.N)
                {
                    Program.player.X = 50;
                    Program.player.Y = 50;
                    Program.isDead = false;
                    Program.waitingForInput = true;
                    Program.playing = true;
                    Program.score = 0;
                    Program.Initialize();
                    return;
                }
                else if (key == ConsoleKey.L)
                {
                    if (airplaneFire.GameEngine.GameSaveManager.LoadGame(out long score, out float playerX, out float playerY))
                    {
                        Program.player.X = playerX;
                        Program.player.Y = playerY;
                        Program.isDead = false;
                        Program.waitingForInput = true;
                        Program.playing = true;
                        Program.score = score;
                        Program.Initialize();
                        return;
                    }
                    else
                    {
                        Console.WriteLine("No saved game found. Starting a new game...");
                        Program.Initialize();
                        return;
                    }
                }
            }
        }
        internal static void UpdateGameState()
        {
            bool u = false;
            bool d = false;
            bool l = false;
            bool r = false;
            bool shoot = false;
            while (Console.KeyAvailable)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape: Program.closeRequested = true; return;
                    case ConsoleKey.Enter: Program.playing = !Program.isDead; return;
                    case ConsoleKey.W or ConsoleKey.UpArrow: u = true; break;
                    case ConsoleKey.A or ConsoleKey.LeftArrow: l = true; break;
                    case ConsoleKey.S or ConsoleKey.DownArrow: d = true; break;
                    case ConsoleKey.D or ConsoleKey.RightArrow: r = true; break;
                    case ConsoleKey.Spacebar: shoot = true; break;
                    case ConsoleKey.P:
                        Program.isPaused = !Program.isPaused;
                        Logger.Log("Game paused");
                        if (Program.isPaused) ConsoleHelper.PromptUserToSave();
                        return;
                }
            }
            if (OperatingSystem.IsWindows())
            {
                if (GetAsyncKeyState((int)ConsoleKey.Escape) is not 0)
                {
                    Program.closeRequested = true;
                    return;
                }

                if (Program.isDead)
                {
                    Program.playing = !(GetAsyncKeyState((int)ConsoleKey.Enter) is not 0);
                }

                u = u || GetAsyncKeyState((int)ConsoleKey.W) is not 0;
                l = l || GetAsyncKeyState((int)ConsoleKey.A) is not 0;
                d = d || GetAsyncKeyState((int)ConsoleKey.S) is not 0;
                r = r || GetAsyncKeyState((int)ConsoleKey.D) is not 0;

                u = u || GetAsyncKeyState((int)ConsoleKey.UpArrow) is not 0;
                l = l || GetAsyncKeyState((int)ConsoleKey.LeftArrow) is not 0;
                d = d || GetAsyncKeyState((int)ConsoleKey.DownArrow) is not 0;
                r = r || GetAsyncKeyState((int)ConsoleKey.RightArrow) is not 0;

                shoot = shoot || GetAsyncKeyState((int)ConsoleKey.Spacebar) is not 0;

                if (Program.waitingForInput)
                {
                    Program.waitingForInput = !(u || d || l || r || shoot);
                }
            }

            if (Program.pauseUpdates || Program.isPaused)
            {
                return;
            }

            if (Program.isDead)
            {
                return;
            }

            if (Program.waitingForInput)
            {
                return;
            }

            Program.update++;

            if (Program.update % 50 == 0)
            {
                Program.SpawnARandomEnemy();
            }

            for (int i = 0; i < Program.playerBullets.Count; i++)
            {
                Program.playerBullets[i].Y++;
            }

            Logger.Log("Updating enemies:");
            foreach (IEnemy enemy in Program.enemies)
            {
                Logger.Log($"Updating {enemy.GetType().Name} at ({enemy.X}, {enemy.Y})");
                enemy.Update();
                Logger.Log($"Updated {enemy.GetType().Name} position to ({enemy.X}, {enemy.Y})");
            }

            Program.player.State = Player.States.None;
            if (l && !r)
            {
                Program.player.X = Math.Max(0, Program.player.X - 1);
                Program.player.State |= Player.States.Left;
            }
            if (r && !l)
            {
                Program.player.X = Math.Min(Program.gameWidth - 1, Program.player.X + 1);
                Program.player.State |= Player.States.Right;
            }
            if (u && !d)
            {
                Program.player.Y = Math.Min(Program.gameHeight - 1, Program.player.Y + 1);
                Program.player.State |= Player.States.Up;
            }
            if (d && !u)
            {
                Program.player.Y = Math.Max(0, Program.player.Y - 1);
                Program.player.State |= Player.States.Down;
            }
            if (shoot)
            {
                Program.playerBullets.Add(new() { X = (int)Program.player.X - 2, Y = (int)Program.player.Y });
                Program.playerBullets.Add(new() { X = (int)Program.player.X + 2, Y = (int)Program.player.Y });
            }

            Program.explodingBullets.Clear();

            for (int i = 0; i < Program.playerBullets.Count; i++)
            {
                PlayerBullet bullet = Program.playerBullets[i];
                bool exploded = false;
                IEnemy[] enemiesClone = Program.enemies.ToArray();
                for (int j = 0; j < enemiesClone.Length; j++)
                {
                    if (enemiesClone[j].CollidingWith(bullet.X, bullet.Y))
                    {
                        if (!exploded)
                        {
                            Program.playerBullets.RemoveAt(i);
                            Program.explodingBullets.Add(bullet);
                            i--;
                            exploded = true;
                        }
                        enemiesClone[j].Shot();
                    }
                }
                if (!exploded && (bullet.X < 0 || bullet.Y < 0 || bullet.X >= Program.gameWidth || bullet.Y >= Program.gameHeight))
                {
                    Program.playerBullets.RemoveAt(i);
                    i--;
                }
            }

            foreach (IEnemy enemy in Program.enemies)
            {
                if (enemy.CollidingWith((int)Program.player.X, (int)Program.player.Y))
                {
                    Program.isDead = true;

                    Logger.Log("Player died");
                    SoundManager.PlayGameOverSound(); // Play game over sound
                    ShowGameOverArt();
                    return;
                }
            }

            Logger.Log("Removing out-of-bounds enemies:");
            for (int i = 0; i < Program.enemies.Count; i++)
            {
                if (Program.enemies[i].IsOutOfBounds())
                {
                    Logger.Log($"Removing {Program.enemies[i].GetType().Name} at ({Program.enemies[i].X}, {Program.enemies[i].Y}) because it is out of bounds");
                    Program.enemies.RemoveAt(i);
                    i--;
                }
            }

            Logger.Log("Current enemies:");
            foreach (IEnemy enemy in Program.enemies)
            {
                Logger.Log($"{enemy.GetType().Name} at ({enemy.X}, {enemy.Y})");
            }
        }

        [LibraryImport("user32.dll")]
        internal static partial short GetAsyncKeyState(int vKey);
    }
    
}
