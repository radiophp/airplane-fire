﻿using airplaneFire.Utility;
using AirPlanFire;
using AirPlanFire.Enemies;
using System;
using System.Text;

namespace airplaneFire.GameEngine
{
    internal static class GameRender
    {
        internal static void RenderFrame(bool showPauseMenu = false)
        {
            const int maxRetryCount = 10;
            int retry = 0;
        Retry:
            if (retry > maxRetryCount)
            {
                return;
            }
            if (Program.consoleWidth != Console.WindowWidth || Program.consoleHeight != Console.WindowHeight)
            {
                Program.consoleWidth = Console.WindowWidth;
                Program.consoleHeight = Console.WindowHeight;
                Console.Clear();
            }
            if (Program.consoleWidth < Program.intendedMinConsoleWidth || Program.consoleHeight < Program.intendedMinConsoleHeight)
            {
                Console.Clear();
                Console.Write($"Console too small at {Program.consoleWidth}w x {Program.consoleHeight}h. Please increase to at least {Program.intendedMinConsoleWidth}w x {Program.intendedMinConsoleHeight}h.");
                Program.pauseUpdates = true;
                return;
            }
            Program.pauseUpdates = false;
            ClearFrameBuffer();
            Program.player.Render();
            foreach (IEnemy enemy in Program.enemies)
            {
                Logger.Log($"Rendering {enemy.GetType().Name} at ({enemy.X}, {enemy.Y})");
                enemy.Render();
            }
            foreach (PlayerBullet bullet in Program.playerBullets)
            {
                if (bullet.X >= 0 && bullet.X < Program.gameWidth && bullet.Y >= 0 && bullet.Y < Program.gameHeight)
                {
                    Program.frameBuffer[bullet.X, bullet.Y] = '^';
                }
            }
            foreach (PlayerBullet explode in Program.explodingBullets)
            {
                if (explode.X >= 0 && explode.X < Program.gameWidth && explode.Y >= 0 && explode.Y < Program.gameHeight)
                {
                    Program.frameBuffer[explode.X, explode.Y] = '#';
                }
            }
            Program.render.Clear();
            Program.render.AppendLine(Program.topBorder);
            for (int y = Program.gameHeight - 1; y >= 0; y--)
            {
                Program.render.Append('┃');
                for (int x = 0; x < Program.gameWidth; x++)
                {
                    Program.render.Append(Program.frameBuffer[x, y]);
                }
                Program.render.AppendLine("┃");
            }
            Program.render.AppendLine(Program.bottomBorder);
            Program.render.AppendLine($"Score: {Program.score}");

            if (Program.waitingForInput)
            {
                Program.render.AppendLine("Press [WASD] or [SPACEBAR] to start...  ");
            }
            if (Program.isDead)
            {
                Program.render.AppendLine("YOU DIED! Press [ENTER] to play again...");

            }
            else if (Program.isPaused && showPauseMenu)
            {
                Program.render.AppendLine("GAME PAUSED! Press [P] to resume...");
                Program.render.AppendLine("Press [S] to save the game now");
            }
            else
            {
                Program.render.AppendLine("                                        ");
                Program.render.AppendLine("Press [P] to pause the game");
            }

            try
            {
                Console.CursorVisible = false;
                Console.SetCursorPosition(0, 0);
                Console.Write(Program.render);
            }
            catch
            {
                retry++;
                goto Retry;
            }
        }

        internal static void ClearFrameBuffer()
        {
            for (int x = 0; x < Program.gameWidth; x++)
            {
                for (int y = 0; y < Program.gameHeight; y++)
                {
                    Program.frameBuffer[x, y] = ' ';
                }
            }
        }
    }
}
