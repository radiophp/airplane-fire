﻿using System;
using System.IO;

namespace airplaneFire.GameEngine
{
    internal static class GameSaveManager
    {


        internal static void SaveGame(long score, float playerX, float playerY)
        {
            try
            {
                using (var fileStream = new FileStream(AppPaths.SaveFilePath, FileMode.Create, FileAccess.Write))
                using (var writer = new StreamWriter(fileStream))
                {
                    writer.WriteLine(score);
                    writer.WriteLine(playerX);
                    writer.WriteLine(playerY);
                }
                ConsoleHelper.DisplayMessage("Game saved successfully!");
            }
            catch (Exception ex)
            {
                ConsoleHelper.DisplayMessage($"Failed to save game: {ex.Message}");
            }
        }

        internal static bool LoadGame(out long score, out float playerX, out float playerY)
        {
            try
            {
                if (File.Exists(AppPaths.SaveFilePath))
                {
                    using StreamReader reader = new StreamReader(AppPaths.SaveFilePath);

                    score = long.Parse(reader.ReadLine());

                    playerX = float.Parse(reader.ReadLine());
                    playerY = float.Parse(reader.ReadLine());
                    return true;
                }
            }
            catch (Exception ex)
            {
                ConsoleHelper.DisplayMessage($"Failed to load game: {ex.Message}");
            }

            score = 0;
            playerX = 0;
            playerY = 0;
            return false;
        }
    }
}
