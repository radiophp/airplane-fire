﻿using System;
using System.IO;

namespace airplaneFire.Utility
{
    internal static class Logger
    {
        //private static readonly string logFilePath = AppPaths.LogFilePath;

        public static void Log(string message)
        {
            string logMessage = $"{DateTime.Now}: {message}";
            //Console.WriteLine(logMessage); // Also print to console for debugging purposes
            try
            {
                using StreamWriter writer = new StreamWriter(AppPaths.LogFilePath, append: true);
                // writer.WriteLine(logMessage);
            }
            catch (IOException ex)
            {
                Console.WriteLine($"Failed to write to log file: {ex.Message}");
            }
        }
    }
}
