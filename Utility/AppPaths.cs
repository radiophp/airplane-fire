﻿using System.IO;
using System;

public static class AppPaths
{
    static AppPaths()
    {
        string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        string appDirectory = Path.Combine(appDataPath, "AirplaneFire");
        Directory.CreateDirectory(appDirectory);

        LogFilePath = Path.Combine(appDirectory, "game.log");
        SaveFilePath = Path.Combine(appDirectory, "game_save.txt");
    }

    public static string LogFilePath { get; private set; }
    public static string SaveFilePath { get; private set; }
}
