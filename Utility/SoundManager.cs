﻿using System;
using System.IO;
using System.Media;

namespace airplaneFire.Utility
{
    internal static class SoundManager
    {
        private static string GetSoundFilePath(string fileName)
        {
            string appDirectory = AppDomain.CurrentDomain.BaseDirectory;
            return Path.Combine(appDirectory, "sounds", fileName);
        }

        public static void PlayExplosionSound()
        {
            try
            {
                string soundFilePath = GetSoundFilePath("explosion.wav");
                using (SoundPlayer player = new SoundPlayer(soundFilePath))
                {
                    player.Load();
                    player.Play();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to play sound: {ex.Message}");
            }
        }

        public static void PlayGameOverSound()
        {
            try
            {
                string soundFilePath = GetSoundFilePath("gameover.wav");
                using (SoundPlayer player = new SoundPlayer(soundFilePath))
                {
                    player.Load();
                    player.Play();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to play sound: {ex.Message}");
            }
        }
    }
}
