﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using airplaneFire;
using airplaneFire.GameEngine;

using airplaneFire.Utility;
using AirPlanFire.Enemies;

namespace AirPlanFire
{
    internal static partial class Program
    {
        internal static bool closeRequested = false;
        internal static Stopwatch stopwatch = new();
        internal static bool pauseUpdates = false;

        internal static int gameWidth = 80;
        internal static int gameHeight = 40;
        internal static int intendedMinConsoleWidth = gameWidth + 3;
        internal static int intendedMinConsoleHeight = gameHeight + 3;
        internal static char[,] frameBuffer = new char[gameWidth, gameHeight];
        internal static string topBorder = '┏' + new string('━', gameWidth) + '┓';
        internal static string bottomBorder = '┗' + new string('━', gameWidth) + '┛';

        internal static int consoleWidth = Console.WindowWidth;
        internal static int consoleHeight = Console.WindowHeight;
        internal static StringBuilder render = new(gameWidth * gameHeight);

        internal static long score = 0;
        internal static int update = 0;
        internal static bool isDead = false;
        internal static Player player = new()
        {
            X = gameWidth / 2,
            Y = gameHeight / 4,
        };
        internal static List<PlayerBullet> playerBullets = new();
        internal static List<PlayerBullet> explodingBullets = new();
        internal static List<IEnemy> enemies = new();
        internal static bool playing = false;
        internal static bool waitingForInput = true;
        internal static bool isPaused = false;

        internal static void Main()
        {
            try
            {
                // Your application logic here
                Logger.Log("Application started.");
            }
            catch (Exception ex)
            {
                Logger.Log($"Unhandled exception: {ex.Message}\n{ex.StackTrace}");
                throw; // Re-throw the exception if necessary
            }

            score = 0;
            ConsoleHelper.InitializeConsole();

            ConsoleHelper.ShowStartScreen();

            while (!closeRequested)
            {
                Initialize();
                while (!closeRequested && playing)
                {
                    GameUpdate.UpdateGameState();
                    if (closeRequested)
                    {
                        return;
                    }
                    Render();
                    ConsoleHelper.SleepAfterRender();
                }
            }
        }

        internal static void Initialize()
        {
            
            update = 0;
            isDead = false;
            player = new Player
            {
                X = gameWidth / 2,
                Y = gameHeight / 4,
            };
            playerBullets = new List<PlayerBullet>();
            explodingBullets = new List<PlayerBullet>();
            enemies = new List<IEnemy>();
            playing = true;
            waitingForInput = true;

            ConsoleHelper.AdjustWindowSize();
        }

        internal static void Render()
        {
            const int maxRetryCount = 10;
            int retry = 0;
        Retry:
            if (retry > maxRetryCount)
            {
                return;
            }
            if (consoleWidth != Console.WindowWidth || consoleHeight != Console.WindowHeight)
            {
                consoleWidth = Console.WindowWidth;
                consoleHeight = Console.WindowHeight;
                Console.Clear();
            }
            if (consoleWidth < intendedMinConsoleWidth || consoleHeight < intendedMinConsoleHeight)
            {
                Console.Clear();
                Console.Write($"Console too small at {consoleWidth}w x {consoleHeight}h. Please increase to at least {intendedMinConsoleWidth}w x {intendedMinConsoleHeight}h.");
                pauseUpdates = true;
                return;
            }
            pauseUpdates = false;
            ClearFrameBuffer();
            player.Render();
            foreach (IEnemy enemy in enemies)
            {
                Logger.Log($"Rendering {enemy.GetType().Name} at ({enemy.X}, {enemy.Y})");
                enemy.Render();
            }
            foreach (PlayerBullet bullet in playerBullets)
            {
                if (bullet.X >= 0 && bullet.X < gameWidth && bullet.Y >= 0 && bullet.Y < gameHeight)
                {
                    frameBuffer[bullet.X, bullet.Y] = '^';
                }
            }
            foreach (PlayerBullet explode in explodingBullets)
            {
                if (explode.X >= 0 && explode.X < gameWidth && explode.Y >= 0 && explode.Y < gameHeight)
                {
                    frameBuffer[explode.X, explode.Y] = '#';
                }
            }
            render.Clear();
            render.AppendLine(topBorder);
            for (int y = gameHeight - 1; y >= 0; y--)
            {
                render.Append('┃');
                for (int x = 0; x < gameWidth; x++)
                {
                    render.Append(frameBuffer[x, y]);
                }
                render.AppendLine("┃");
            }
            render.AppendLine(bottomBorder);
            render.AppendLine(  $"Score: {score}"  );

            if (waitingForInput)
            {
                render.AppendLine(  "Press [WASD] or [SPACEBAR] to start...  "  );
            }
            if (isDead)
            {
                render.AppendLine(  "YOU DIED! Press [ENTER] to play again..."  );
            }
            else if (isPaused)
            {
                render.AppendLine(  "GAME PAUSED! Press [P] to resume..."  );
                render.AppendLine(  "Press [S] to save the game now"  );
            }
            else
            {
                render.AppendLine("                                        ");
                render.AppendLine(  "Press [P] to pause the game"  );
            }

            try
            {
                Console.CursorVisible = false;
                Console.SetCursorPosition(0, 0);
                Console.Write(render);
            }
            catch
            {
                retry++;
                goto Retry;
            }
        }

        internal static void ClearFrameBuffer()
        {
            for (int x = 0; x < gameWidth; x++)
            {
                for (int y = 0; y < gameHeight; y++)
                {
                    frameBuffer[x, y] = ' ';
                }
            }
        }

        internal static void SpawnARandomEnemy()
        {
            IEnemy enemy;
            switch (Random.Shared.Next(4))
            {
                case 0:
                    enemy = new Tank(Random.Shared.Next(gameWidth), gameHeight, 0, -0.1f);
                    Logger.Log($"Spawned Tank at ({enemy.X}, {enemy.Y})");
                    break;
                case 1:
                    enemy = new UFO1(Random.Shared.Next(gameWidth), gameHeight, 0, -0.1f);
                    Logger.Log($"Spawned UFO1 at ({enemy.X}, {enemy.Y})");
                    break;
                case 2:
                    enemy = new UFO2(Random.Shared.Next(gameWidth), gameHeight, 0, -0.1f);
                    Logger.Log($"Spawned UFO2 at ({enemy.X}, {enemy.Y})");
                    break;
                default:
                    // Adjust the XVelocity for horizontal movement
                    enemy = new Helicopter(-Helicopter.XMax, Random.Shared.Next(gameHeight), 0.1f, 0); // Moving right
                    Logger.Log($"Spawned Helicopter at ({enemy.X}, {enemy.Y})");
                    break;
            }
            enemies.Add(enemy);
            Logger.Log($"Spawned {enemy.GetType().Name}");
        }
    }
}
