; -- Inno Setup script --

[Setup]
; General
AppName=AirplaneFire
AppVersion=1.0
DefaultDirName={pf}\AirplaneFire
DefaultGroupName=AirplaneFire
OutputBaseFilename=AirplaneFireSetup
Compression=lzma
SolidCompression=yes
LicenseFile=doc\license.rtf

; Information
SetupIconFile=output.ico
AppCopyright=Mohammad Inanloo
AppPublisher=Mohammad Inanloo

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Files]
; Include all files from the publish directory
Source: "bin\Release\net8.0\win-x64\publish\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{group}\AirplaneFire"; Filename: "{app}\airplaneFire.exe"; IconFilename: "{app}\output.ico"
Name: "{group}\Uninstall AirplaneFire"; Filename: "{uninstallexe}"

[Run]
; Ensure .NET runtime is installed if not using self-contained deployment
; Filename: "path\to\dotnet-runtime-5.0.10-win-x64.exe"; Parameters: "/install /quiet /norestart"; Flags: waituntilterminated skipifdoesntexist

Filename: "{app}\airplaneFire.exe"; Description: "{cm:LaunchProgram,AirplaneFire}"; Flags: nowait postinstall skipifsilent
