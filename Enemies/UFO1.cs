﻿using System;
using System.Linq;
using airplaneFire.Utility;

namespace AirPlanFire.Enemies
{
    internal class UFO1 : Enemy
    {
        private static readonly string[] ufo1Sprite =
        {
            @" _!_ ",
            @"(_o_)",
            @" ''' ",
        };

        static UFO1()
        {
            XMax = ufo1Sprite.Max(s => s.Length);
            YMax = ufo1Sprite.Length;
        }

        public UFO1(float x, float y, float xVelocity, float yVelocity)
            : base(50, x, y, xVelocity, yVelocity, 200) // Set scorePerKill to 200
        {
            this.Sprite = ufo1Sprite;
            Logger.Log($"Spawned UFO1 at ({X}, {Y})");
        }

        public override void Render()
        {
            Logger.Log($"Rendering UFO1 at ({X}, {Y}) with sprite height {Sprite.Length} and width {Sprite[0].Length}");
            for (int y = 0; y < Sprite.Length; y++)
            {
                int yo = (int)Y + y;
                if (yo >= 0 && yo < Program.frameBuffer.GetLength(1))
                {
                    for (int x = 0; x < Sprite[y].Length; x++)
                    {
                        int xo = (int)X + x;
                        if (xo >= 0 && xo < Program.frameBuffer.GetLength(0))
                        {
                            if (Sprite[y][x] != ' ')
                            {
                                Program.frameBuffer[xo, yo] = Sprite[y][x];
                                Logger.Log($"Rendered '{Sprite[y][x]}' at ({xo}, {yo})");
                            }
                        }
                    }
                }
            }
        }

        public override void Update()
        {
            X += XVelocity;
            Y += YVelocity;
            Logger.Log($"Updated UFO1 position to ({X}, {Y})");
        }

        public override bool CollidingWith(int x, int y)
        {
            int xo = x - (int)X;
            int yo = y - (int)Y;
            return yo >= 0 && yo < Sprite.Length && xo >= 0 && xo < Sprite[yo].Length && Sprite[yo][xo] != ' ';
        }

        public override bool IsOutOfBounds()
        {
            return X < -XMax || X > Program.gameWidth || Y < -YMax || Y > Program.gameHeight;
        }
    }
}
