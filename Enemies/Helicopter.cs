﻿using System;
using System.Linq;

namespace AirPlanFire.Enemies
{
    internal class Helicopter : Enemy
    {
        private static readonly string[] spriteA =
        {
            @"     -'-`-   ",
            @"'\===<[_]L)  ",          
            @"  ~~~~~+~~~~~",
        };

        private static readonly string[] spriteB =
        {
            @"     -'-`-   ",
            @"*\===<[_]L)  ",
            @"  -----+-----",
        };

        static Helicopter()
        {
            XMax = Math.Max(spriteA.Max(s => s.Length), spriteB.Max(s => s.Length));
            YMax = Math.Max(spriteA.Length, spriteB.Length);
        }

        public Helicopter(float x, float y, float xVelocity, float yVelocity)
            : base(70, x, y, xVelocity, yVelocity, 100) // Set scorePerKill to 100
        {
            Sprite = spriteA;
            // Logger.Log($"Spawned Helicopter at ({X}, {Y})");
        }

        public override void Render()
        {
            //  Logger.Log($"Rendering Helicopter at ({X}, {Y})");
            for (int y = 0; y < Sprite.Length; y++)
            {
                int yo = (int)Y + y;
                if (yo >= 0 && yo < Program.frameBuffer.GetLength(1))
                {
                    for (int x = 0; x < Sprite[y].Length; x++)
                    {
                        int xo = (int)X + x;
                        if (xo >= 0 && xo < Program.frameBuffer.GetLength(0))
                        {
                            if (Sprite[y][x] != ' ')
                            {
                                Program.frameBuffer[xo, yo] = Sprite[y][x];
                            }
                        }
                    }
                }
            }
        }

        public override void Update()
        {
            Frame++;
            if (Frame > 10)
            {
                Sprite = Sprite == spriteA ? spriteB : spriteA;
                Frame = 0;
            }
            X += XVelocity;
            Y += YVelocity;
            //  Logger.Log($"Updated Helicopter position to ({X}, {Y})");
        }

        public override bool CollidingWith(int x, int y)
        {
            int xo = x - (int)X;
            int yo = y - (int)Y;
            return yo >= 0 && yo < Sprite.Length && xo >= 0 && xo < Sprite[yo].Length && Sprite[yo][xo] != ' ';
        }

        public override bool IsOutOfBounds()
        {
            return X < -XMax || X > Program.gameWidth || Y < -YMax || Y > Program.gameHeight;
        }
    }
}

