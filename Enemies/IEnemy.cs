﻿using System;

namespace AirPlanFire.Enemies
{
    internal interface IEnemy
    {
        int Health { get; }
        float X { get; }
        float Y { get; }
        void Render();
        void Update();
        bool CollidingWith(int x, int y);
        bool IsOutOfBounds();
        void Shot();
    }
}
