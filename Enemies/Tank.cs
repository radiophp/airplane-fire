﻿using System;
using System.Linq;
using airplaneFire.Utility;

namespace AirPlanFire.Enemies
{
    internal class Tank : Enemy
    {
        private static readonly string[] spriteDown =
        {
            @" ___ ",
            @"|_O_|",
            @"[ooo]",
        };

        private static readonly string[] spriteUp =
        {
            @" _^_ ",
            @"|___|",
            @"[ooo]",
        };

        private static readonly string[] spriteLeft =
        {
            @"  __ ",
            @"=|__|",
            @"[ooo]",
        };

        private static readonly string[] spriteRight =
        {
            @" __  ",
            @"|__|=",
            @"[ooo]",
        };

        static Tank()
        {
            XMax = Math.Max(spriteDown.Max(s => s.Length), Math.Max(spriteUp.Max(s => s.Length), Math.Max(spriteLeft.Max(s => s.Length), spriteRight.Max(s => s.Length))));
            YMax = Math.Max(spriteDown.Length, Math.Max(spriteUp.Length, Math.Max(spriteLeft.Length, spriteRight.Length)));
        }

        public Tank(float x, float y, float xVelocity, float yVelocity)
            : base(100, x, y, xVelocity, yVelocity, 150) // Set scorePerKill to 150
        {
            UpdateSprite();
            Logger.Log($"Spawned Tank at ({X}, {Y})");
        }

        private void UpdateSprite()
        {
            if (XVelocity > 0)
            {
                Sprite = spriteRight;
            }
            else if (XVelocity < 0)
            {
                Sprite = spriteLeft;
            }
            else if (YVelocity > 0)
            {
                Sprite = spriteUp;
            }
            else if (YVelocity < 0)
            {
                Sprite = spriteDown;
            }
        }

        public override void Render()
        {
            Logger.Log($"Rendering Tank at ({X}, {Y})");
            for (int y = 0; y < Sprite.Length; y++)
            {
                int yo = (int)Y + y;
                if (yo >= 0 && yo < Program.frameBuffer.GetLength(1))
                {
                    for (int x = 0; x < Sprite[y].Length; x++)
                    {
                        int xo = (int)X + x;
                        if (xo >= 0 && xo < Program.frameBuffer.GetLength(0))
                        {
                            if (Sprite[y][x] != ' ')
                            {
                                Program.frameBuffer[xo, yo] = Sprite[y][x];
                            }
                        }
                    }
                }
            }
        }

        public override void Update()
        {
            X += XVelocity;
            Y += YVelocity;
            UpdateSprite();
            Logger.Log($"Updated Tank position to ({X}, {Y})");
        }

        public override bool CollidingWith(int x, int y)
        {
            int xo = x - (int)X;
            int yo = y - (int)Y;
            return yo >= 0 && yo < Sprite.Length && xo >= 0 && xo < Sprite[yo].Length && Sprite[yo][xo] != ' ';
        }

        public override bool IsOutOfBounds()
        {
            return X < -XMax || X > Program.gameWidth || Y < -YMax || Y > Program.gameHeight;
        }
    }
}
