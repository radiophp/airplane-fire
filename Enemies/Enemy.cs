﻿using System;
using airplaneFire.Utility;

namespace AirPlanFire.Enemies
{
    internal abstract class Enemy : IEnemy
    {
        public int Health { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public float XVelocity { get; protected set; }
        public float YVelocity { get; protected set; }
        protected int Frame { get; set; }
        protected string[] Sprite { get; set; } = Array.Empty<string>();

        public static int XMax { get; protected set; }
        public static int YMax { get; protected set; }

        public int ScorePerKill { get; protected set; }

        public Enemy(int health, float x, float y, float xVelocity, float yVelocity, int scorePerKill)
        {
            Health = health;
            X = x;
            Y = y;
            XVelocity = xVelocity;
            YVelocity = yVelocity;
            ScorePerKill = scorePerKill;
        }

        public abstract void Render();

        public abstract void Update();

        public abstract bool CollidingWith(int x, int y);

        public abstract bool IsOutOfBounds();

        public virtual void Shot()
        {
            Health--;
            if (Health <= 0)
            {
                Program.enemies.Remove(this);
                Program.score += ScorePerKill;
                SoundManager.PlayExplosionSound();
            }
        }
    }
}
