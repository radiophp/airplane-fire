﻿using System;
using System.Linq;
using airplaneFire.Utility;

namespace AirPlanFire.Enemies
{
    internal class UFO2 : Enemy
    {
        private static readonly string[] ufo2Sprite =
        {
            @"     _!_     ",
            @"    /_O_\    ",
            @"-==<_‗_‗_>==-",
        };

        static UFO2()
        {
            XMax = ufo2Sprite.Max(s => s.Length);
            YMax = ufo2Sprite.Length;
        }

        public UFO2(float x, float y, float xVelocity, float yVelocity)
            : base(80, x, y, xVelocity, yVelocity, 250) // Set scorePerKill to 250
        {
            this.Sprite = ufo2Sprite;
            Logger.Log($"Spawned UFO2 at ({X}, {Y})");
        }

        public override void Render()
        {
            Logger.Log($"Rendering UFO2 at ({X}, {Y}) with sprite height {Sprite.Length} and width {Sprite[0].Length}");

            string colorCode = "\x1b[32m";  // Green
            string resetCode = "\x1b[0m";   // Reset

            for (int y = 0; y < Sprite.Length; y++)
            {
                int yo = (int)Y + y;
                if (yo >= 0 && yo < Program.frameBuffer.GetLength(1))
                {
                    for (int x = 0; x < Sprite[y].Length; x++)
                    {
                        int xo = (int)X + x;
                        if (xo >= 0 && xo < Program.frameBuffer.GetLength(0))
                        {
                            if (Sprite[y][x] != ' ')
                            {

                                Program.frameBuffer[xo, yo] = Sprite[y][x];
                            }
                        }
                    }
                }
            }
        
        }

        public override void Update()
        {
            X += XVelocity;
            Y += YVelocity;
            Logger.Log($"Updated UFO2 position to ({X}, {Y})");
        }

        public override bool CollidingWith(int x, int y)
        {
            int xo = x - (int)X;
            int yo = y - (int)Y;
            return yo >= 0 && yo < Sprite.Length && xo >= 0 && xo < Sprite[yo].Length && Sprite[yo][xo] != ' ';
        }

        public override bool IsOutOfBounds()
        {
            return X < -XMax || X > Program.gameWidth || Y < -YMax || Y > Program.gameHeight;
        }
    }
}
