# AirPlanFire 

AirPlanFire is a vertically scrolling shooter game.
```
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃                                                            ┃
┃                           ___                              ┃
┃                          |_O_|                             ┃
┃                          [ooo]                             ┃
┃                                                            ┃
┃                                                            ┃
┃                                                            ┃
┃                            -----+-----                     ┃
┃                          *\===<[_]L)                       ┃
┃                               -'-`-           -----+-----  ┃
┃       -----+-----                           *\===<[_]L)    ┃
┃     *\===<[_]L)                                  -'-`-     ┃
┃          -'-`-                                             ┃
┃                                                            ┃
┃                                _!_                         ┃
┃                               /_O_\                        ┃
┃                           -==<_‗_‗_>==-                    ┃
┃                                                            ┃
┃                                                            ┃
┃          _!_                                               ┃
┃         (_o_)                                              ┃
┃          '''                                   __          ┃
┃                                              =|__|         ┃
┃                                              [ooo]         ┃
┃                                                            ┃
┃                                   ^   ^                    ┃
┃                                   ^   ^                    ┃
┃                                   ^   ^                    ┃
┃                                   ^   ^                    ┃
┃                                                            ┃
┃                                    ╱‾╲                     ┃
┃     __                            ╱╱‾╲╲                    ┃
┃    |__|=                         ╱'╲O╱'╲                   ┃
┃    [ooo]                        ╱ / ‾ \ ╲                  ┃
┃                                 ╲_╱───╲_╱                  ┃
┃                                                            ┃
┃                                                            ┃
┃                                                            ┃
┃                                                            ┃
┃                                                            ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
```
## Author

Mohammad Inanloo



## Input

- `W`, `A`, `S`, `D`, `↑`, `↓`, `←`, `→`: movement
- `spacebar`: shoot
- `enter`: start new game
- `escape`: exit game

## Downloads

[win-x64](https://gitlab.com/radiophp/airplane-fire/-/raw/main/Installation.exe?ref_type=heads&inline=false)

~linux-x64~ (not supported)

~osx-x64~ (not supported)

## Project Structure

- **airplaneFire.sln**: Solution file for the AirplaneFire project.
- **airplaneFire.csproj**: Project file for the AirplaneFire application.
- **AirplaneFire.iss**: Inno Setup script for the installation process.
- **Installation.exe**: Executable for installing the game on Windows.
- **output.ico**: Icon used in the application.
- **Program.cs**: Main entry point for the application.
- **Player.cs**: Class representing the player character.
- **PlayerBullet.cs**: Class representing the player's bullets.
- **Enemy.cs**: Base class for enemies in the game.
- **Helicopter.cs**: Class representing helicopter enemies.
- **IEnemy.cs**: Interface for enemy classes.
- **Tank.cs**: Class representing tank enemies.
- **UFO1.cs**: Class representing the first type of UFO enemies.
- **UFO2.cs**: Class representing the second type of UFO enemies.
- **ConsoleHelper.cs**: Helper class for console-related operations.
- **GameRender.cs**: Class handling game rendering.
- **GameSaveManager.cs**: Class managing game saves.
- **GameUpdate.cs**: Class handling game updates.
- **AppPaths.cs**: Class for managing application paths.
- **Logger.cs**: Logging utility for the application.
- **SoundManager.cs**: Class handling game sounds.
- **User32_dll.cs**: Class for interacting with User32.dll for Windows-specific functionality.

